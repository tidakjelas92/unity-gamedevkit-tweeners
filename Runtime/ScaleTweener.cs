using System;
using DG.Tweening;
using UnityEngine;

namespace UMNP.GamedevKit.Tweeners
{
    /// <summary>
    /// Tweens basic scale animation of a rect transform.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class ScaleTweener : BaseTweener
    {
        private RectTransform _rectTransform;
        private Vector3 _anchor = Vector3.one;

        protected override void Awake()
        {
            base.Awake();
            _rectTransform = GetComponent<RectTransform>();
            _anchor = _rectTransform.localScale;
        }

        /// <summary>
        /// Scale away from the anchor to given scale. Anchor is the original starting scale of the transform.
        /// </summary>
        public void ScaleFrom(in Vector3 to, in TweenKey tweenKey, in Action onStart = null, in Action onComplete = null)
        {
            if (FromTween == null)
                FromTween = CreateTween(_anchor, to, tweenKey, onComplete);

            PlayFromTween(onStart);
        }

        /// <summary>
        /// Scale towards the anchor from given scale. Anchor is the original starting scale of the transform.
        /// </summary>
        public void ScaleTo(in Vector3 from, in TweenKey tweenKey, in Action onStart = null, in Action onComplete = null)
        {
            if (ToTween == null)
                ToTween = CreateTween(from, _anchor, tweenKey, onComplete);

            PlayToTween(onStart);
        }

        /// <summary>
        /// Scale with a punch tween.
        /// </summary>
        public void Punch(in PunchTweenKey punchTweenKey, in Action onStart = null, Action onComplete = null)
        {
            if (FromTween == null)
            {
                FromTween = CreatePunchTween(
                    punchTweenKey,
                    () =>
                    {
                        _rectTransform.localScale = _anchor;
                        onComplete?.Invoke();
                    }
                );
            }

            PlayFromTween(onStart);
        }

        private Tweener CreateTween(in Vector3 from, in Vector3 to, in TweenKey tweenKey, Action onComplete = null)
        {
            var tweener = _rectTransform.DOScale(to, tweenKey.duration)
                .From(from)
                .SetEase(tweenKey.ease)
                .SetUpdate(IgnoreTime)
                .SetAutoKill(false)
                .OnComplete(() => onComplete?.Invoke());

            return tweener;
        }

        private Tweener CreatePunchTween(in PunchTweenKey punchTweenKey, Action onComplete = null)
        {
            var tweener = _rectTransform.DOPunchScale(punchTweenKey.punch, punchTweenKey.duration, punchTweenKey.vibrato, punchTweenKey.elasticity)
                .SetUpdate(IgnoreTime)
                .SetAutoKill(false)
                .OnComplete(() => onComplete?.Invoke());

            return tweener;
        }
    }
}

using System;
using UnityEngine;

namespace UMNP.GamedevKit.Tweeners
{
    [Serializable]
    public struct PunchTweenKey
    {
        public float duration;
        public float elasticity;
        public Vector3 punch;
        public int vibrato;

        public PunchTweenKey(in Vector3 punch, float duration, int vibrato = 10, float elasticity = 1.0f)
        {
            this.duration = duration;
            this.elasticity = elasticity;
            this.punch = punch;
            this.vibrato = vibrato;
        }
    }
}

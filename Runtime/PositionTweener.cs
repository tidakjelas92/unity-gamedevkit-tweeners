﻿using System;
using DG.Tweening;
using UnityEngine;

namespace UMNP.GamedevKit.Tweeners
{
    /// <summary>
    /// Tweens basic position animation of a rect transform.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class PositionTweener : BaseTweener
    {
        private RectTransform _rectTransform;
        private Vector2 _anchor = Vector2.zero;

        protected override void Awake()
        {
            base.Awake();
            _rectTransform = GetComponent<RectTransform>();
            _anchor = _rectTransform.anchoredPosition;
        }

        /// <summary>
        /// Move away from the anchor with offset. Anchor is the original starting position of the transform.
        /// </summary>
        public void MoveFrom(in Vector2 offset, in TweenKey tweenKey, in Action onStart = null, in Action onComplete = null)
        {
            if (FromTween == null)
                FromTween = CreateTween(_anchor, _anchor - offset, tweenKey, onComplete);

            PlayFromTween(onStart);
        }

        /// <summary>
        /// Move towards the anchor with absolute offset. Anchor is the original starting position of the transform.
        /// </summary>
        public void MoveTo(in Vector2 offset, in TweenKey tweenKey, in Action onStart = null, in Action onComplete = null)
        {
            if (ToTween == null)
                ToTween = CreateTween(_anchor + offset, _anchor, tweenKey, onComplete);

            PlayToTween(onStart);
        }

        /// <summary>
        /// Move with a punch tween.
        /// </summary>
        public void Punch(in PunchTweenKey punchTweenKey, in Action onStart = null, Action onComplete = null)
        {
            if (FromTween == null)
            {
                FromTween = CreatePunchTween(punchTweenKey, () =>
                    {
                        _rectTransform.anchoredPosition = _anchor;
                        onComplete?.Invoke();
                    });
            }

            PlayFromTween(onStart);
        }

        private Tweener CreateTween(in Vector2 from, in Vector2 to, in TweenKey tweenKey, Action onComplete = null)
        {
            var tweener = _rectTransform.DOAnchorPos(to, tweenKey.duration)
                .From(from)
                .SetEase(tweenKey.ease)
                .SetUpdate(IgnoreTime)
                .SetAutoKill(false)
                .OnComplete(() => onComplete?.Invoke());

            return tweener;
        }

        private Tweener CreatePunchTween(in PunchTweenKey punchTweenKey, Action onComplete = null)
        {
            var tweener = _rectTransform.DOPunchAnchorPos(punchTweenKey.punch, punchTweenKey.duration, punchTweenKey.vibrato, punchTweenKey.elasticity)
                .SetUpdate(IgnoreTime)
                .SetAutoKill(false)
                .OnComplete(() => onComplete?.Invoke());

            return tweener;
        }
    }
}
